#!/usr/bin/env zsh

function filedir() {
    local -r itempath="${1}"
    local -r itemline="${2}"

    if [ -f "${itempath}" ]; then
        exa --color=always --long --group "${itempath}"
        if [ -z "${itemline}" ]; then
            bat --color=always --style=numbers,grid "${itempath}"
        else
            bat --color=always --style=numbers,grid --highlight-line="${itemline}" "${itempath}"
        fi
    elif [ -d "${itempath}" ]; then
        echo "${itempath}"
        exa --color=always --long --group --all --sort=name "${itempath}" | bat --style=numbers,grid --color=always
    fi
}

function dironly() {
    local -r itempath="${1}"

    # Note:
    # - BSD tools do not have long options.
    if [ -f "${itempath}" ]; then
        local -r range=10
        local -r itemdir=$(dirname ${itempath})
        local -r itemname=$(basename "${itempath}")
        local -r itemindex=$(exa --oneline --sort=name "${itemdir}" | rg --line-regexp --fixed-strings --line-number "${itemname}" | cut -d : -f 1)
        local -r minindex=$(( itemindex-range > 0 ? itemindex-range : 0 ))
        local -r maxindex=$(( itemindex+range ))
        echo "${itemdir}"
        exa --color=always --long --group --all --sort=name "${itemdir}" | bat --highlight-line "${itemindex}" --line-range "${minindex}:${maxindex}" --style=numbers,grid --color=always
    elif [ -d "${itempath}" ]; then
        echo "${itempath}"
        exa --color=always --long --group --all --sort=name "${itempath}" | bat --style=numbers,grid --color=always
    fi
}

function usage() {
    [ -z "${1}" ] || echo "${1}"
    echo "$(basename "${0}") <mode> <path>"
    echo "    mode: --file-dir | --dir-only"
}

# if [ $# -ne 2 ] || [ -z "${1}" ] || [ -z "${2}" ]; then
#     usage
#     exit 1
# fi

case "${1}" in
    --file-dir)
        filedir "${2}" "${3}"
        ;;

    --dir-only)
        dironly "${2}"
        ;;

    *)
        echo "Invalid mode"
        usage "----"
        exit 1
        ;;
esac
