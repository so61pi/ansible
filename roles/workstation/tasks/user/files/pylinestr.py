#!/usr/bin/python3

import sys

if len(sys.argv) != 2:
    exit(1)

query = sys.argv[1]
if len(query) == 0:
    query = "l"

for line in sys.stdin:
    l = line.strip()
    try:
        print(eval(query))
    except Exception as e:
        print("{}".format(e))
