# This file is read for interactive shell after `.zprofile`
# and should contain things for interactive sessions.

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH
  export PATH=$HOME/bin:$PATH

# Path to your oh-my-zsh installation.
  export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Auto start tmux.
# ZSH_TMUX_AUTOSTART="true"

autoload -U select-word-style
select-word-style bash

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
  bgnotify
  # tmux
  # zsh-autosuggestions

  # zsh-syntax-highlighting must be at the end of plugin list
  # https://github.com/zsh-users/zsh-syntax-highlighting#why-must-zsh-syntax-highlightingzsh-be-sourced-at-the-end-of-the-zshrc-file
  zsh-syntax-highlighting
)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

#################################################################
#                                                               #
#                         CUSTOM CONFIG                         #
#                                                               #
#################################################################
# Save commands to history as-is.
HISTFILE=${HOME}/.zshhist
HISTSIZE=1000000000                     # Number of lines of history in memory.
SAVEHIST=${HISTSIZE}                    # Number of lines of history in history file.
setopt      inc_append_history          # Immediately append to the history file, not just when a term is killed.
unsetopt    hist_expire_dups_first      #
unsetopt    hist_ignore_space           #
setopt      hist_ignore_dups            # Ignore adjacent duplicated commands.
setopt      hist_reduce_blanks          # Remove superfluous blanks before saving.
setopt      hist_find_no_dups           # Display no duplicated commands when searching backward.
setopt      hist_verify                 # Edit recalled history before running.
setopt      extended_history            # Save timestamp on history entries.
setopt      extended_glob               # Enable extended glob.
setopt      auto_cd                     # Enable auto cd.
setopt      auto_pushd                  # Auto push current directory each time you cd
unsetopt    share_history               # Disable share history across terminals.
bindkey -e                              # Emacs mode.

###########################[ ENV VAR ]###########################

# Shared directory between host and VMs.
export VMSHAREDIR="${HOME}/Downloads/vmshare/"
hash -d vmshare="${VMSHAREDIR}"

# Keep less' output on the screen after it exits.
export LESS=-XFRS

if hash hx 2>/dev/null; then
  export EDITOR=$(which hx)
elif hash vim 2>/dev/null; then
  export EDITOR=$(which vim)
elif hash nano 2>/dev/null; then
  export EDITOR=$(which nano)
fi

export RUST_BACKTRACE=full

############################[ ALIAS ]############################

hash -d dw="${HOME}/w/"
hash -d dp="${HOME}/w/programming/projects/"
hash -d ds="${HOME}/w/programming/sources/"
hash -d dt="${HOME}/w/tmp/"
alias cdw="cd ~dw"
alias cdp="cd ~dp"
alias cds="cd ~ds"
alias cdt="cd ~dt"
alias cdg='cd "$(git rev-parse --show-toplevel)"'
alias llt='ls -ltr'
alias lrg="rg --no-heading"
alias b=bat
if hash xclip 2>/dev/null; then
  alias pbcopy="xclip -r -selection c"
  alias pbpaste="xclip -selection clipboard -o"
fi

######################[ EXTERNAL PROGRAMS ]######################

if hash zoxide 2>/dev/null; then
  export _ZO_ECHO=1
  eval "$(zoxide init zsh)"
  alias cdi=zi
  function cd() {
    if [ "$#" -ge 2 ]; then
      # More than 2 arguments to `cd` (e.g., `cd a b`)
      z "$@"
    elif [ "$#" -lt 1 ] || [ -z "${1}" ]; then
      # No argument, or 1st argument is empty
      z
    elif [ -f "${1}" ]; then
      # When we have only one argument and it is a file, we cd into the containing directory
      echo "cd into $(dirname "${1}")"
      z $(dirname "${1}")
    else
      # Only one argument which is not a file
      z "${1}"
    fi
  }
else
  function cd() {
    if [ "$#" -ge 2 ]; then
      # More than 2 arguments to `cd` (e.g., `cd a b`)
      builtin cd "$@"
    elif [ "$#" -lt 1 ] || [ -z "${1}" ]; then
      # No argument, or 1st argument is empty
      builtin cd
    elif [ -f "${1}" ]; then
      # When we have only one argument and it is a file, we cd into the containing directory
      echo "cd into $(dirname "${1}")"
      builtin cd $(dirname "${1}")
    else
      # Only one argument which is not a file
      builtin cd "${1}"
    fi
  }
fi

if hash starship 2>/dev/null; then
  eval "$(starship init zsh)"
fi

if hash atuin 2>/dev/null; then
  export ATUIN_NOBIND="true"
  eval "$(atuin init zsh)"
fi

# FILE=/usr/share/doc/fzf/examples/key-bindings.zsh && test -f "${FILE}" && source "${FILE}"
# FILE=~/.fzf.zsh && test -f "${FILE}" && source "${FILE}"
# FILE=~/.pet.zsh && test -f "${FILE}" && source "${FILE}"
# FILE=$HOME/w/tools/python-tools/bin/activate && test -f "${FILE}" && source "${FILE}"

# Load key bindings for fzf.
# http://zsh.sourceforge.net/Doc/Release/Functions.html#Autoloading-Functions
autoload fzf-key-bindings; fzf-key-bindings
export FZF_DEFAULT_OPTS='--color hl:#ff7700,hl+:#00ffff'
bindkey "${terminfo[kcuu1]}" fzf-history-widget # Press up key to open fzf history search.

#################################################################
#                                                               #
#                        CUSTOM COMMANDS                        #
#                                                               #
#################################################################

function xyz::cmd::rg() { rg --color=always --line-number "$@" }
function xyz::sep::rg() { awk -F : '{print $2 "|" $1}' } # 123|filename-can-have-space

function xyz::cmd::fd() { fd --color=always "$@" }
function xyz::sep::fd() { awk '{print "0|" $1}' } # 0|filename-can-have-space

function xyz::fzfcmd() { FZF_DEFAULT_OPTS="--query=\"${1}\" ${FZF_DEFAULT_OPTS}" fzf --cycle --ansi --delimiter : --reverse --preview='fd-fzf-helper.zsh --file-dir {1} {2}' --preview-window='top:70%:+{2}-5' --multi }

function xyz::act::bat() { if [ -z "${2}" ]; then print -z bat "${1}"; else print -z bat --highlight-line="${2}" "${1}"; fi }
function xyz::act::helix() { if [ -z "${2}" ]; then print -z hx "${1}"; else print -z hx "${1}:${2}"; fi }
function xyz::act::vim() { if [ -z "${2}" ]; then print -z vim "${1}"; else print -z vim "${1}" "+${2}"; fi }
function xyz::act::subl() { if [ -z "${2}" ]; then print -z subl "${1}"; else print -z subl "${1}:${2}"; fi }
function xyz::act::code() { if [ -z "${2}" ]; then print -z code "${1}"; else print -z code --goto "${1}:${2}"; fi }
function xyz::act::HexFiend() { print -z HexFiend "${1}" }
function xyz::act::xdgopen() { print -z xdg-open "${1}" }
function xyz::act::open() { print -z open "${1}" }
function xyz::act::pbcopy() { echo -n "${1}" | pbcopy }
function xyz::act::cp() { print -z "cp ${1} " }
function xyz::act::mv() { print -z "mv ${1} " }
function xyz::act::lns() { print -z "ln -s ${1} " }
function xyz::act::scp() { print -z "scp ${1}" }
function xyz::act::list() { print -z "ls -alh ${1}" }
function xyz::act::cd() { print -z "cd ${1}" }
function xyz::act::printnext() { print -z "${1}" }
function xyz::act::git-blame() { print -z "git blame ${1}" }
function xyz::act::git-log() { print -z "git log --graph --stat ${1}" }

function xyz::foldwithline() {
  typeset -r foldedtxt="${1}"
  typeset -r fname="${2}"
  if [ -z "${fname}" ]; then
    echo "${foldedtxt}"
  else
    typeset -r fline="${3}"
    if [ -z "${fline}" ]; then
      printf "%s %q" "${foldedtxt}" "${fname}"
    else
      printf "%s %q" "${foldedtxt}" "${fname}:${fline}"
    fi
  fi
}

function xyz::foldnoline() {
  typeset -r foldedtxt="${1}"
  typeset -r fname="${2}"
  if [ -z "${fname}" ]; then
    echo "${foldedtxt}"
  else
    printf "%s %q" "${foldedtxt}" "${fname}"
  fi
}

function xyz::foldsingledir() {
  if [ ! -z "${1}" ]; then
    echo "${1}"
    return
  fi

  if [ -z "${2}" ]; then
    echo ""
    return
  fi

  if [ -f "${2}" ]; then
    printf %q $(dirname "${2}")
  else
    printf %q "${2}"
  fi
}

function xyz::menu() {
  typeset -r previewfile="${1}"

  #
  ## Create and show menu.
  #
  typeset -r menusep="--------------------"
  typeset -a actstrs=() actcmds=() actfolds=()

  actstrs+=("Open in Sublime Text [subl]");     actcmds+=("xyz::act::subl");        actfolds+=("xyz::foldwithline")
  actstrs+=("Open in VSCode [code]");           actcmds+=("xyz::act::code");        actfolds+=("")
  actstrs+=("${menusep}");                      actcmds+=("false");                 actfolds+=("")

  actstrs+=("Open in Helix");                   actcmds+=("xyz::act::helix");       actfolds+=("xyz::foldwithline")
  actstrs+=("Open in Vim");                     actcmds+=("xyz::act::vim");         actfolds+=("")
  actstrs+=("${menusep}");                      actcmds+=("false");                 actfolds+=("")

  actstrs+=("Open in Bat");                     actcmds+=("xyz::act::bat");         actfolds+=("")
  actstrs+=("${menusep}");                      actcmds+=("false");                 actfolds+=("")

  actstrs+=("Open in HexFiend");                actcmds+=("xyz::act::HexFiend");    actfolds+=("")
  actstrs+=("Open with open");                  actcmds+=("xyz::act::open");        actfolds+=("")
  actstrs+=("Open with xdg-open");              actcmds+=("xyz::act::xdgopen");     actfolds+=("xyz::foldwithline")
  actstrs+=("${menusep}");                      actcmds+=("false");                 actfolds+=("")

  actstrs+=("Copy to clipboard [pbcopy]");      actcmds+=("xyz::act::pbcopy");      actfolds+=("xyz::foldwithline")
  actstrs+=("${menusep}");                      actcmds+=("false");                 actfolds+=("")

  actstrs+=("Copy [cp]");                       actcmds+=("xyz::act::cp");          actfolds+=("xyz::foldnoline")
  actstrs+=("Move [mv]");                       actcmds+=("xyz::act::mv");          actfolds+=("xyz::foldnoline")
  actstrs+=("Symlink [ln]");                    actcmds+=("xyz::act::lns");         actfolds+=("")
  actstrs+=("SSH Copy [scp]");                  actcmds+=("xyz::act::scp");         actfolds+=("xyz::foldnoline")
  actstrs+=("List entries [ls/ll]");            actcmds+=("xyz::act::list");        actfolds+=("xyz::foldnoline")
  actstrs+=("${menusep}");                      actcmds+=("false");                 actfolds+=("")

  actstrs+=("Change directory [cd]");           actcmds+=("xyz::act::cd");          actfolds+=("xyz::foldsingledir")
  actstrs+=("Preview directory [ppp]");         actcmds+=("ppp");                   actfolds+=("xyz::foldsingledir")
  actstrs+=("${menusep}");                      actcmds+=("false");                 actfolds+=("")

  actstrs+=("Print to next command");           actcmds+=("xyz::act::printnext");   actfolds+=("xyz::foldwithline")
  actstrs+=("${menusep}");                      actcmds+=("false");                 actfolds+=("")

  actstrs+=("Git blame");                       actcmds+=("xyz::act::git-blame");   actfolds+=("")
  actstrs+=("Git log");                         actcmds+=("xyz::act::git-log");     actfolds+=("")

  typeset -a actmenu=()
  typeset -A actions=()
  for ((i = 1, j = 1; i <= $#actstrs; i++)); do
    if [ "${actstrs[${i}]}" = "${menusep}" ]; then
      local txt="$(printf "%2s  %s" "  " ${actstrs[${i}]})"
    else
      local txt="$(printf "%2s. %s" ${j} ${actstrs[${i}]})"
      j=$(( j+1 ))
    fi
    actmenu+=("${txt}")
    actions+=("${txt}" "${i}")
  done

  while true; do
    if [ ! -z "${previewfile}" ] && [ -f "${previewfile}" ]; then
      typeset selectedact=$(for txt in ${actmenu}; do echo "${txt}"; done | fzf --reverse --cycle --preview-window="up:5" --preview="cat \"${previewfile}\"")
    else
      typeset selectedact=$(for txt in ${actmenu}; do echo "${txt}"; done | fzf --reverse --cycle --preview-window="up:5")
    fi
    [ -z "${selectedact}" ] && return 1
    case "${selectedact}" in
      "")
        return 1
        ;;
      *"${menusep}")
        continue
        ;;
      *)
        echo "${actcmds[${actions[${selectedact}]}]}" "${actfolds[${actions[${selectedact}]}]}"
        return 0
        ;;
    esac
  done
}

function p() {
  typeset -r initquery="$@"
  #
  ## Select command from history.
  #
  typeset -r allowedcmds="rg|fd"
  typeset -r selectedcmd=$(fc -lnr -500 | { rg "^(${allowedcmds})( |$)" | uniq | head -n 10 } | fzf --height=10 --reverse)
  [ -z "${selectedcmd}" ] && return 1

  typeset -r cmd="xyz::cmd::${selectedcmd}"
  typeset -r sep="xyz::sep::${selectedcmd}"

  #
  ## Run enhanced command.
  #
  typeset -r itemsfile=$(mktemp /tmp/pp.output.XXXXXXXXXX)
  trap "rm -f $itemsfile" EXIT

  typeset -a fnames=() flines=()
  eval ${cmd} | xyz::fzfcmd "${initquery}" | eval ${sep} > "${itemsfile}"
  if [ ! -s "${itemsfile}" ]; then return 1; fi

  xyz::menu "${itemsfile}" | read -r actcmd actfold
  [ -z "${actcmd}" ] && return 1

  # https://stackoverflow.com/questions/42559948/bash-read-file-path-from-another-file-and-open-with-vim
  if [ -z "${actfold}" ]; then
    while IFS='|' read -r fline fname <&3; do
      ${actcmd} "${fname}" "${fline}"
    done 3< "${itemsfile}"
  else
    typeset foldedtxt=""
    while IFS='|' read -r fline fname <&3; do
      foldedtxt=$(${actfold} "${foldedtxt}" "${fname}" "${fline}")
    done 3< "${itemsfile}"
    [ -z "${foldedtxt}" ] && return 1
    ${actcmd} "${foldedtxt}"
  fi
}

function pp() {
  #
  ## Select command from history.
  #
  typeset -r ignoredcmds="pp|ppp|rm|dd|sudo[[:blank:]]+rm|sudo[[:blank:]]+dd|exec"
  typeset -r selectedcmd="$(fc -lnr -100 | uniq | rg --invert-match "^[[:blank:]]*(${ignoredcmds})([[:blank:]]|$)" | fzf --height=30 --reverse)"
  [ -z "${selectedcmd}" ] && return 1

  typeset -r tmpfile=$(mktemp /tmp/pp.output.XXXXXXXXXX)
  trap "rm -f $tmpfile" EXIT
  typeset -r output=$(eval ${selectedcmd})
  [ -z "${output}" ] && return 1
  echo "${output}" > "${tmpfile}"

  #
  ## Select query.
  #
  # 1st line is query, subsequent lines are selected items.
  typeset -r tmpstr=$(cat "${tmpfile}" | fzf --cycle --phony --print-query --reverse --query="l.split()" --preview="expr {n} + 1; cat \"${tmpfile}\" | pylinestr {q} | bat --style=plain --color=always --highlight-line=\$(expr {n} + 1)" --multi)
  typeset -a tmparr=("${(f)tmpstr}")
  [ $#tmparr -eq 0 ] && return 1

  # If there is no query, default to get entire line.
  local query="${tmparr[1]}"
  [ -z "${query}" ] && query="l"

  #
  ## Save result to file.
  #
  typeset -r itemsfile=$(mktemp /tmp/pp.output.XXXXXXXXXX)
  trap "rm -f $itemsfile" EXIT
  for ((i = 2; i <= $#tmparr; i++)); do
    typeset processedtxt=$(echo "${tmparr[${i}]}" | pylinestr "${query}")
    if [ -z "${processedtxt}" ]; then continue; fi
    echo "${processedtxt}" >> "${itemsfile}"
  done
  if [ ! -s "${itemsfile}" ]; then return 1; fi

  #
  ## Act.
  #
  xyz::menu "${itemsfile}" | read -r actcmd actfold
  [ -z "${actcmd}" ] && return 1

  # https://stackoverflow.com/questions/42559948/bash-read-file-path-from-another-file-and-open-with-vim
  if [ -z "${actfold}" ]; then
    while IFS= read -r line <&3; do
      ${actcmd} "${line}"
    done 3< "${itemsfile}"
  else
    typeset foldedtxt=""
    while IFS= read -r line <&3; do
      foldedtxt=$(${actfold} "${foldedtxt}" "${line}")
    done 3< "${itemsfile}"
    [ -z "${foldedtxt}" ] && return 1
    ${actcmd} "${foldedtxt}"
  fi
}

function ppp() {
  if [ -z "${1}" ]; then
    exa --color=always --oneline --sort=name | fzf --cycle --layout=reverse --ansi --preview 'fd-fzf-helper.zsh --file-dir {}' --preview-window='top:70%'
  else
    (cd "${1}" && exa --color=always --oneline --sort=name | fzf --cycle --layout=reverse --ansi --preview 'fd-fzf-helper.zsh --file-dir {}' --preview-window='top:70%')
  fi
}

function llts() {
  typeset -r itemsfile=$(mktemp /tmp/pp.output.XXXXXXXXXX)
  trap "rm -f $itemsfile" EXIT

  # Regex to extract `.rwxr-xr-x   15M so61pi 19 Jun 12:54 file name` to `file name`.
  # `?` is to make the regex non-greedy.
  typeset -r repattern='s/.*? [ 123][0-9] [A-Z][a-z]{2} [0-2][0-9]:[0-6][0-9] //'
  if [ -z "${1}" ]; then
    typeset -r selected=$(exa --color=always --long --sort=modified --reverse | fzf --ansi --reverse --cycle --height=10 --multi | perl -pe "${repattern}")
  else
    typeset -r selected=$(builtin cd "${1}" && exa --color=always --long --sort=modified --reverse | fzf --ansi --reverse --cycle --height=10 --multi | perl -pe "${repattern}" | sed "s|^|${1}/|")
  fi

  if [ -z "${selected}" ]; then return 1; fi
  echo "${selected}" > "${itemsfile}"

  xyz::menu "${itemsfile}" | read -r actcmd actfold
  [ -z "${actcmd}" ] && return 1

  # https://stackoverflow.com/questions/42559948/bash-read-file-path-from-another-file-and-open-with-vim
  if [ -z "${actfold}" ]; then
    while IFS= read -r line <&3; do
      ${actcmd} "${line}"
    done 3< "${itemsfile}"
  else
    typeset foldedtxt=""
    while IFS= read -r line <&3; do
      foldedtxt=$(${actfold} "${foldedtxt}" "${line}")
    done 3< "${itemsfile}"
    [ -z "${foldedtxt}" ] && return 1
    ${actcmd} "${foldedtxt}"
  fi
}

function _hist-parts-helper() {
  for line in $(fc -ln 0); do
    for part in ${line}; do
      echo "${part}"
    done
  done
}

function hist-parts-reload() {
  _hist-parts-helper | awk '!a[$0]++' > /tmp/zsh-hist-parts.txt
}

function hist-parts() {
  # If file size is empty or the file is already older than 30 minutes.
  if [ ! -s /tmp/zsh-hist-parts.txt ] || [ $(( $(date +%s) - $(date -r /tmp/zsh-hist-parts.txt +%s) )) -ge 1800 ]; then
    hist-parts-reload
  fi
  cat /tmp/zsh-hist-parts.txt | fzf --ansi --reverse --cycle --height=10 --multi
}

function _hist-parts-widget() {
  local result=$(hist-parts)
  local ret=$?
  LBUFFER+=${result}
  zle reset-prompt
  return $ret
}

zle -N _hist-parts-widget
bindkey '^L' _hist-parts-widget

if [ "${TERM_PROGRAM}" = "iTerm.app" ]; then
  ### Color for iTerm2 tabs
  function tabcolorimp {
    echo -n -e "\033]6;1;bg;red;brightness;$1\a"
    echo -n -e "\033]6;1;bg;green;brightness;$2\a"
    echo -n -e "\033]6;1;bg;blue;brightness;$3\a"
  }

  function tabcolorrnd {
    tabcolorimp $(jot -r 1 0 255) $(jot -r 1 0 255) $(jot -r 1 0 255)
  }

  function tabcolorpwd {
    local -r pwdhash=$(pwd | md5)
    tabcolorimp $((16#${pwdhash[1,2]})) $((16#${pwdhash[3,4]})) $((16#${pwdhash[5,6]}))
  }

  autoload -U add-zsh-hook
  add-zsh-hook chpwd tabcolorpwd
  tabcolorpwd
fi

function cdlast() {
  local -r lastdir=$(ls | tail -1)
  if [ -z "${lastdir}" ]; then
    return 1
  fi
  cd "${lastdir}"
}

function mdcd() {
  if [ "$#" -ne 1 ]; then
    echo "Usage: mdcd <directory>"
    return 1
  else
    md "${1}" && cd "${1}" && pwd
  fi
}

function e() {
  echo "\n\n\n\n\n"
}

# This must be placed at the end of this file, otherwise zellij will not have all the above config.
# if hash zellij 2>/dev/null; then
#   export ZELLIJ_AUTO_ATTACH=true
#   export ZELLIJ_AUTO_EXIT=true
#   eval "$(zellij setup --generate-auto-start zsh)"
# fi
